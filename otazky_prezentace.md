# Otázky na prezentaci

**Zodpovězte prosím pro třídu pravdivě následující otázky**
| otázka | vyjádření |
| :-------------------------------------------- | :------------------------------ |
| jak dlouho mi tvorba zabrala - **čistý čas** | 10 h |
| odkud jsem čerpal inspiraci | https://youtu.be/d_V-QmnB_as?si=xj5l0QiOwWNXDv-1 |
| odkaz na video | https://youtu.be/6dfyGKrw1LA |
| jak se mi to podařilo rozplánovat | Vše na poslední chvíli |
| proč jsem zvolil tento design | líbil se mi nejvíc z návrhů, co jsem našel |
| zapojení | https://gitlab.spseplzen.cz/gabriell/mood_lamp_gabriell/-/blob/main/Dokumentace/schema/Schema.PNG |
| z jakých součástí se zapojení skládá | PVC trubky, 6x kabel, wemos, ARGB pásek a DTH11 senzor|
| realizace | https://gitlab.spseplzen.cz/gabriell/mood_lamp_gabriell/-/blob/main/Dokumentace/fotky/4.jpg |
| UI | 10.202.31.142:1880/ui |
| co se mi povedlo | Nevím |
| co se mi nepovedlo/příště bych udělal/a jinak | Nevím, asi bych použil lepší materiál |
| zhodnocení celé tvorby (návrh známky) | za mě jsem svojí Mood Lampu udělal v pohodě, takže bych si dal 2-3 |
