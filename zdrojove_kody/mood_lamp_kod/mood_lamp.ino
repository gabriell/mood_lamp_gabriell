#include <ESP8266WiFi.h>
#include <PubSubClient.h>
#include <Adafruit_NeoPixel.h>
#include "DHT.h"

//rgb pasek
int r = 0;
int g = 0;
int b = 0;

int status = 1;
int modes = 1; //1 - normal, 2 - random colors
#define pinDIN D3
#define pocetLED 40

#define pinDHT D4
#define typDHT11 DHT11
DHT mojeDHT(pinDHT, typDHT11);

Adafruit_NeoPixel rgbWS = Adafruit_NeoPixel(pocetLED, pinDIN, NEO_GRB + NEO_KHZ800);

// Update these with values suitable for your network.

const char* ssid = "KPZ";
const char* password = "mameradisport";
const char* mqtt_server = "broker.hivemq.com";

WiFiClient espClient;
PubSubClient client(espClient);
unsigned long lastMsg = 0;
#define MSG_BUFFER_SIZE  (50)
char msg[MSG_BUFFER_SIZE];
int value = 0;

//funkce pro nastavení barvy čtverce
void nastavRGB (byte r, byte g, byte b) {
  uint32_t barva;
  barva = rgbWS.Color(r, g, b);

  for (int i = 0; i < pocetLED; i++) {
    rgbWS.setPixelColor(i, barva);
  }

  rgbWS.show();
}

void setup_wifi() {
  mojeDHT.begin();

  delay(10);
  // We start by connecting to a WiFi network
  Serial.println();
  Serial.print("Connecting to ");
  Serial.println(ssid);

  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);

  while (WiFi.status() != WL_CONNECTED) {
    delay(500);
    Serial.print(".");
  }

  randomSeed(micros());

  Serial.println("");
  Serial.println("WiFi connected");
  Serial.println("IP address: ");
  Serial.println(WiFi.localIP());
}

void callback(char* topic, byte* payload, unsigned int length) {
  Serial.print("Message arrived [");
  Serial.print(topic);
  Serial.print("] ");
  for (int i = 0; i < length; i++) {
    Serial.print((char)payload[i]);
  }
  
  //on a off
  if (strcmp(topic, "Gaba_Lamp/modes") == 0){
      modes = atoi((char*)payload);
  } 

  //modes
  if (strcmp(topic, "Gaba_Lamp/on_off") == 0){
      status = atoi((char*)payload);
  } 

  if (status == 1){
    //funkce atoi převede znaky na řetězec
    
    if (modes == 1 || modes == 3){
      if (strcmp(topic, "Gaba_Lamp/rgb/r") == 0){
        r = atoi((char*)payload);
      } 

      if (strcmp(topic, "Gaba_Lamp/rgb/g") == 0){
        g = atoi((char*)payload);
      }   

      if (strcmp(topic, "Gaba_Lamp/rgb/b") == 0){
        b = atoi((char*)payload);
      } 
      nastavRGB(r, g, b);
    }

  }
  else{
    nastavRGB(0, 0, 0);
  }

  // Switch on the LED if an 1 was received as first character
  if ((char)payload[0] == '1') {
    digitalWrite(BUILTIN_LED, LOW);   // Turn the LED on (Note that LOW is the voltage level
    // but actually the LED is on; this is because
    // it is active low on the ESP-01)
  } else {
    digitalWrite(BUILTIN_LED, HIGH);  // Turn the LED off by making the voltage HIGH
  }

}

void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Create a random client ID
    String clientId = "ESP8266Client-";
    clientId += String(random(0xffff), HEX);
    // Attempt to connect
    if (client.connect(clientId.c_str())) {
      Serial.println("connected");
      // Once connected, publish an announcement...
      client.publish("outTopic", "hello world");
      // ... and resubscribe
      client.subscribe("/#");
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}

void setup() {
  rgbWS.begin();
  pinMode(BUILTIN_LED, OUTPUT);     // Initialize the BUILTIN_LED pin as an output
  Serial.begin(115200);
  setup_wifi();
  client.setServer(mqtt_server, 1883);
  client.setCallback(callback);
}

void loop() {
  float tep = mojeDHT.readTemperature();
  float vlh = mojeDHT.readHumidity();

  if (!client.connected()) {reconnect();}
  client.loop();

  unsigned long now = millis();
  if (now - lastMsg > 2000) {
    lastMsg = now;
    if (!isnan(tep)) {
      client.publish("Gaba_Lamp/dth11/teplota", String(tep).c_str());
    }
    if (!isnan(vlh)) {
      client.publish("Gaba_Lamp/dth11/vlhkost", String(vlh).c_str());
    }
  }
  if (modes == 2 && status == 1){
    randomColorEffect();
  }
}


void randomColorEffect() {
  const unsigned long updateInterval = 500;  // Aktualizovat barvu každých 100 ms
  static unsigned long lastUpdateTime = 0;

  unsigned long currentTime = millis();

  if (currentTime - lastUpdateTime >= updateInterval) {
    lastUpdateTime = currentTime;

    // Generování náhodných hodnot pro R, G, B
    int randomR = random(256);
    int randomG = random(256);
    int randomB = random(256);

    // Nastavení náhodné barvy pro všechny LED diody
    nastavRGB(randomR, randomG, randomB);
  }
}
